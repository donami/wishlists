import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as nodemailer from "nodemailer";
import * as nodemailerSendgrid from "nodemailer-sendgrid";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

// exports.helloWorld = functions.https.onRequest((request, response) => {
//   response.send('Hello from Firebase!');
// });

admin.initializeApp();

const transporter = nodemailer.createTransport(
  nodemailerSendgrid({
    apiKey: functions.config().sg.apikey,
  })
);

exports.onNewWishlistCreated = functions.firestore
  .document("sharedLists/{id}")
  .onCreate((snap, context) => {
    const data = snap.data();
    const { recipients /*wishlistId*/ } = data;
    // const {
    //   params: { id },
    // } = context;

    // console.log(context);
    // console.log('data', wishlistId);
    // console.log('recipients', recipients);
    // console.log(id);

    const mailPromises = recipients.map((recipient: string) => {
      const options = {
        from: "cnat21@gmail.com",
        to: recipient,
        subject: "Wishify - Access granted to a wishlist!",
        html: `
        <img src="https://firebasestorage.googleapis.com/v0/b/fir-797a4.appspot.com/o/email-header.jpg?alt=media&token=3b64cfb4-789c-4f64-83bb-a018bd843246" alt=""/>
        
        <h1>Shared wishlist at Wishify!</h1>
        <p>You have been given access to view a Wishlist</p>
        <p>Click the link below to view the list</p>

        <p>
          Keep in mind that the version of this list is <em>private</em> and only
          available to a select few chosen by the creator. <br /><br/>
        </p>
        
        <p>
          You have the possiblity to "reserve" items on the list, meaning that you intend to purchase it. <br/>
          The creator of the list should not be given access to the link in this mail, to prevent them from seeing reserved items.
        </p>

        <p>
          Head over to use at <em>Wishify</em> and start gift shopping with a breeze.
          Click <a href="https://fir-797a4.web.app/#/shared/${snap.id}" target="_blank">here</a> to get started
        </p>
        `,

        // -------

        // <h3>Debug:</h3>
        // <pre>
        // <code>
        // ${JSON.stringify(data)}
        // </code>
        // </pre>`,
      };

      return transporter.sendMail(options);
    });

    // const mailOptions = {
    //   from: 'cnat21@gmail.com',
    //   to: 'd0namiz@gmail.com',
    //   subject: 'A new note is created',
    //   html: `
    //     <h1>Shared list!</h1>
    //     <p>You have been given access to view a Wishlist</p>
    //     <p>Click the link below to view the list</p>

    //     <p>
    //       Keep in mind that the version of this list is <em>private</em> and only
    //       available to the persons chosen by the creator. <br /><br/>
    //     </p>

    //     <p>
    //       You have the possiblity to "reserve" items on the list, meaning that you are going to purchase it.
    //       The creator of the list should not be given access to the link in this mail, to prevent them from seeing reserved items.
    //     </p>

    //     <p>
    //       Click here to get started:
    //     </p>

    //     <a href="https://fir-797a4.web.app/#/shared/${snap.id}" target="_blank">Wishlist</a>

    //     -------

    //     <h3>Debug:</h3>
    //     <pre>
    //     <code>
    //     ${JSON.stringify(data)}
    //     </code>
    //     </pre>`,
    // };

    // return transporter
    //   .sendMail(mailOptions)
    //   .then(() => console.log('Email Sent!'))
    //   .catch((error: any) => console.error(error));
    // return Promise.resolve(true);

    return Promise.all(mailPromises)
      .then(() => {
        console.log("Emails Sent!");
      })
      .catch((error: any) => console.error(error));
  });
