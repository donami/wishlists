import {
  collection,
  getDocs,
  query,
  where,
  addDoc,
  doc,
  updateDoc,
  deleteDoc,
  getDoc,
  arrayUnion,
} from "firebase/firestore";
import { auth, db } from "./firebase";
import { WishlistItem } from "./types";

export const getWishlistsForUser = async () => {
  const userId = auth.currentUser?.uid;
  if (!userId) {
    return [];
  }

  const wishlistCol = collection(db, "wishlists");
  const wishlistQuery = query(wishlistCol, where("userId", "==", userId));

  const snapshot = await getDocs(wishlistQuery);
  const data = snapshot.docs.map(async (doc) => {
    const itemsRef = collection(db, "wishlists", doc.id, "items");
    const itemsSnapshot = await getDocs(itemsRef);

    const items = itemsSnapshot.docs.map((itemDoc) => {
      return {
        id: itemDoc.id,
        ...itemDoc.data(),
      };
    });

    return {
      ...doc.data(),
      id: doc.id,
      items,
    };
  });

  return Promise.all(data);
};

export const addNewItemToList = async (listId: string, item: WishlistItem) => {
  const userId = auth.currentUser?.uid;

  if (!userId) {
    return;
  }

  const ref = await addDoc(collection(db, "wishlists", listId, "items"), item);
  return ref.id;
};

export const updateWishlistName = async (listId: string, name: string) => {
  return updateDoc(doc(db, "wishlists", listId), { title: name });
};

export const deleteItemFromList = async (listId: string, itemId: string) => {
  return deleteDoc(doc(db, "wishlists", listId, "items", itemId));
};

export const updateItemInList = async (listId: string, item: WishlistItem) => {
  return updateDoc(doc(db, "wishlists", listId, "items", item.id), {
    title: item.title,
    image: item.image,
    url: item.url,
    price: item.price,
  });
};

export const addNewWishlist = async (data: { title: string }) => {
  const userId = auth.currentUser?.uid;

  if (!userId) {
    return;
  }

  const item = {
    title: data.title,
    userId,
  };

  const ref = await addDoc(collection(db, "wishlists"), item);
  return {
    id: ref.id,
    ...item,
  };
};

export const deleteWishlist = async (listId: string) => {
  return deleteDoc(doc(db, "wishlists", listId));
};

export const shareWishlist = async (recipients: string[], listId: string) => {
  const item = {
    recipients,
    wishlistId: listId,
  };

  const sharedListRef = collection(db, "sharedLists");
  const sharedListQuery = query(
    sharedListRef,
    where("wishlistId", "==", listId)
  );
  const snapshot = await getDocs(sharedListQuery);

  if (snapshot.size) {
    // if list already is being shared, simply add the new recipient(s) to the data
    const promises = snapshot.docs.map((document) => {
      return updateDoc(doc(db, "sharedLists", document.id), {
        recipients: arrayUnion(...recipients),
      });
    });
    await Promise.all(promises);
  } else {
    const ref = await addDoc(collection(db, "sharedLists"), item);
    return {
      id: ref.id,
      ...item,
    };
  }
};

export const fetchSharedList = async (sharedId: string) => {
  const ref = doc(db, "sharedLists", sharedId);
  const snap = await getDoc(ref);

  if (!snap.exists()) {
    throw new Error("Shared list not found!");
  }

  const data = snap.data();

  const wishlistRef = doc(db, "wishlists", data.wishlistId);
  const wishlistSnapshot = await getDoc(wishlistRef);

  const { reserved = [] } = data;

  const getItems = async (wishlistId: string) => {
    const itemsRef = collection(db, "wishlists", wishlistId, "items");
    const itemsSnapshot = await getDocs(itemsRef);

    const items = itemsSnapshot.docs.map((itemDoc) => {
      return {
        id: itemDoc.id,
        ...itemDoc.data(),
      };
    });
    return items;
  };
  const items = await getItems(wishlistSnapshot.id);

  return {
    ...wishlistSnapshot.data(),
    id: snap.id,
    wishlistId: wishlistSnapshot.id,
    items,
    reserved,
  };
};

export const reserveItem = async (
  sharedListId: string,
  itemId: string,
  reservedBy: string,
  reservedOn: Date
) => {
  const ref = doc(db, "sharedLists", sharedListId);

  await updateDoc(ref, {
    reserved: arrayUnion({
      itemId,
      reservedBy: reservedBy,
      reservedOn: reservedOn,
    }),
  });
};

export const fetchSharedWith = async (listId: string) => {
  // const ref = doc(db, "sharedLists");

  const col = collection(db, "sharedLists");
  const sharedListQuery = query(col, where("wishlistId", "==", listId));

  const snapshot = await getDocs(sharedListQuery);

  if (snapshot.size === 0) {
    return [];
  }

  const [sharedList] = snapshot.docs;
  const data = sharedList.data();
  return data.recipients || [];
};
